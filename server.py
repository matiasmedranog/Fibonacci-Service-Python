from flask import Flask
from math import sqrt
import os, json

app = Flask(__name__)

@app.route('/')
@app.route('/<number>', methods=['POST'])
def showMessage(number):
    response = int(((1+sqrt(5))**int(number)-(1-sqrt(5))**int(number))/(2**int(number)*sqrt(5)))
    return json.dumps({'n':int(response),'n-1':int(response-1)})

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
