import pytest, requests

class TestFibonacci(object):

    # Tests Fibonacci 

    @pytest.mark.skip(reason="Stop")
    def testFibonacci3(self):
        response = requests.post('http://127.0.0.1:5000/3')

        assert response.status_code == 200
        assert response.text == '{"n-1": 1, "n": 2}' 

    @pytest.mark.skip(reason="Stop")
    def testFibonacci5(self):
        response = requests.post('http://127.0.0.1:5000/5')

        assert response.status_code == 200
        assert response.text == '{"n-1": 4, "n": 5}' 

    @pytest.mark.skip(reason="Stop")
    def testFibonacci7(self):
        response = requests.post('http://127.0.0.1:5000/7')

        assert response.status_code == 200
        assert response.text == '{"n-1": 12, "n": 13}' 
