FROM python:2.7
ADD . /app
WORKDIR /app
RUN pip install Flask requests pytest
EXPOSE 5000
ENTRYPOINT python server.py 
